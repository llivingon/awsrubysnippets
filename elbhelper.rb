class ELBHelper
  def initialize()
    @elbClientV1 = Aws::ElasticLoadBalancing::Client.new()
    @elbClientV2 = Aws::ElasticLoadBalancingV2::Client.new()
   end

  #This is a private sub-routine
  def _getClientObject(elbType="ClassicELB")
    if elbType.include? "ClassicELB"
      return @elbClientV1
    elsif elbType.include? "ApplicationELB"
      return @elbClientV2
    else
      raise Exception.new("elbType can be ClassicELB or ApplicationELB")
    end
  end

  #getAllELBsOnAVpc returns a list of ELB Names in a VPC
  #IMPORTANT- If elbType=="ClassicELB", we get back only Classic Load Balancers
  #IMPORTANT- If elbType=="ApplicationELB", we get back only Application Load Balancers
  #IMPORTANT- To get back all Load Balancers, call getAllELBsOnAVpc twice first with elbType="ClassicELB" and the again elbType="ApplicationELB"
  def getAllELBsOnAVpc(elbType)
    elbobj = _getClientObject(elbType)
    elbList = []
    elbobj.describe_load_balancers()[0].each do |i|
    elbList.push(i.to_h[:load_balancer_name])
    end
    return elbList
  end

  #getELBArn returns a ELB ARN when povided an ELB Name
  #IMPORTANT- getELBArn works only for Application ELB |Reason: Only ElasticLoadBalancingV2 returns ARN
  #Example: elbObj.getELBArn("test-digital-web-systest-alb")
  #Returns: arn:aws:elasticloadbalancing:eu-west-1:00820218606:loadbalancer/app/test-digital-web-systest-alb/802bdc542bef6038
  def getELBArn(elbName)
    elbobj = _getClientObject("ApplicationELB")
    elbobj.describe_load_balancers()[0].each do |i|
      if i.to_h[:load_balancer_name] == elbName
        return i.to_h[:load_balancer_arn]
      end
    end
    raise Exception.new("ELB #{elbName} not found on VPC")
  end

  #This subroutine returns true if an elb is distributed across all AZs
  #Throws an exception if given elb is not found on VPC
  #Throws an exception if given elb is not distributed across all AZs
  #IMPORTANT- If elbType=="ClassicELB", we can verify only Classic Load Balancers
  #IMPORTANT- If elbType=="ApplicationELB", we can verify only Application Load Balancers
  #Example: elbObj.verifyELBIsAcrossAllAZs("us-digital-web-systest-asd", "ApplicationELB")
  #Returns: true
  def verifyELBIsAcrossAllAZs(elbName, elbType)
    elbFound = false
    elbobj = _getClientObject(elbType)
    availableAZs = ['eu-west-1a','eu-west-1b']
    elbobj.describe_load_balancers()[0].each do |i|
      if i.to_h[:load_balancer_name] == elbName
        elbFound = true
        i.to_h[:availability_zones].each do |j|
          if elbType.include? "ApplicationELB"
            if availableAZs.include?(j[:zone_name])
              availableAZs.delete(j[:zone_name])
            else
              raise Exception.new("Unknown Zone found #{j.to_s}")
            end
            elsif elbType.include? "ClassicELB"
            if availableAZs.include?(j)
              availableAZs.delete(j)
            else
              raise Exception.new("Unknown Zone found #{j.to_s}")
            end
          end
        end
      end
    end
    if elbFound == false
      raise Exception.new("elb #{elbName} Not Found ")
    end
    if availableAZs.length==0
      return true
    else
      raise Exception.new("elb #{elbName} Not distributed on AZ #{availableAZs.to_s}")
    end
  end

  #getELBListener returns an array of Listeners
  #IMPORTANT- getELBListener returns different aray, depending on elbType
  #Example: elbObj.getELBListener("us-digital-web-dystest-asd", "ApplicationELB")
  #Classic Returns: {:protocol=>"TCP", :load_balancer_port=>80, :instance_protocol=>"TCP", :instance_port=>80}
  #Application Returns: {:listener_arn=>"arn:aws:elasticloadbalancing:eu-west-1:300820918606:listener/ac428514e4755c0",
    #:load_balancer_arn=>"arn:aws:elasticloadbalancing:eu-west-1:300820918606:loadbalancer/app/uk-digital-web-systest-alb/502bdc542bef2038", :port=>443, :protocol=>"HTTPS",
    #:certificates=>[{:certificate_arn=>"arn:aws:acm:eu-we18d9"}], :ssl_policy=>"ELBSecurityPolicy-2016-08", :default_actions=>[{:type=>"forward", :target_group_arn=>"arn:aws:elasticl6e"}]}
  def getELBListener(elbName, elbType)
    elbobj = _getClientObject(elbType)
    if elbType.include? "ApplicationELB"
      elbArn = getELBArn(elbName)
      return elbobj.describe_listeners({ load_balancer_arn: elbArn, }).to_h[:listeners]
    elsif elbType.include? "ClassicELB"
      elbListenerArray = []
      elbobj.describe_load_balancers({ load_balancer_names: [ elbName, ], }).to_h[:load_balancer_descriptions].each do |i|
         i[:listener_descriptions].each do |j|
           elbListenerArray.push(j[:listener])
        end
      end
      return elbListenerArray
    end
  end

  #getClassicElbEC2Instances returns an array of EC2 Instances for a Classic ELB 
  #Example: elbObj.getClassicElbEC2Instances("test-ha-asda-test-web-mob")
  #Returns: ['i-12e47c99ace24175b','i-45e47a99cce12275b']
  def getClassicElbEC2Instances(elbName)
      elbInstanceArray = []
      @elbClientV1.describe_load_balancers({ load_balancer_names: [ elbName, ], }).to_h[:load_balancer_descriptions].each do |i|
        i[:instances].each do |j|
          elbInstanceArray.push(j[:instance_id]) 
        end
      end  
      return elbInstanceArray
  end

  #getClassicELBSubnets returns an array of Subnets for a Classic ELB 
  #Example: elbObj.getClassicELBSubnets("test-ha-nonprod-test-web-mob")
  #Returns: ['subnet-1f2dfe3b','subnet-1f2dfe4b']
  def getClassicELBSubnets(elbName)
      elbSubnetArray = []
      @elbClientV1.describe_load_balancers({ load_balancer_names: [ elbName, ], }).to_h[:load_balancer_descriptions].each do |i|
        i[:subnets].each do |j|
          elbSubnetArray.push(j) 
        end
      end  
      return elbSubnetArray
  end

  #getClassicELBSecurityGroups returns an array of SecurityGroups for a Classic ELB 
  #Example: elbObj.getClassicELBSecurityGroups("test-ha-nonprod-test-web-mob")
  #Returns: ['sg-2eb24678','sg-2eb24567']
  def getClassicELBSecurityGroups(elbName)
      elbSecurityGroupArray = []
      @elbClientV1.describe_load_balancers({ load_balancer_names: [ elbName, ], }).to_h[:load_balancer_descriptions].each do |i|
        i[:security_groups].each do |j|
          elbSecurityGroupArray.push(j) 
        end
      end  
      return elbSecurityGroupArray
  end
  
end

