class CloudWatchHelper
  def initialize()
    @cloudwatchClient = Aws::CloudWatchLogs::Client.new()
  end

  #This subroutine Lists log events from the specified log stream
  #By default, this operation returns as many as 200 log events
  def getlogEvents(logGroupName, logStreamName)
    return @cloudwatchClient.get_log_events({
      log_group_name: "#{logGroupName}",
      log_stream_name: "#{logStreamName}",
      limit: 200,
      })[0]
  end

  #This subroutine returns hash of log stream details - creation_time, first_event_timestamp, last_event_timestamp etc for the specified log group
  #This operation returns as many as 1 log stream hash
  def describelogEvents(logGroupName, logStreamName)
    return @cloudwatchClient.describe_log_streams({
      log_group_name: "#{logGroupName}",
      log_stream_name_prefix: "#{logStreamName}",
      descending: true,
      }).to_h[:log_streams]
  end

end
