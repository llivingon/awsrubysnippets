lass S3Helper
  def initialize(awsEnvironment, awsRole)
    @s3Resource = Aws::S3::Resource.new()
    @s3Client = Aws::S3::Client.new()
  end

  #subroutine to determine whether a Bucket Exists
  def determineIfBucketExists(bucketName)
    bucket_exists = false
    begin
      resp = @s3Client.head_bucket({bucket: bucketName, use_accelerate_endpoint: false})
      bucket_exists = true
    rescue
    end
    return bucket_exists
  end

  #getBuckets returns a list of Buckets in a VPC
  def getBuckets()
    bucketList = []
    @s3Client.list_buckets({}).to_h[:buckets].each do |i|
      bucketList.push(i[:name])
    end
    return bucketList
  end

  #getObjectsInABucket retuns a list of Items in a bucket
  #retuns an array of items (max 100)
  #IMPORTANT- This returns item from entire bucket, if you know from which folder you need to get items from, use subroutine getItemsInAFolder
  def getObjectsInABucket(bucketName)
    ec2List = []
    if determineIfBucketExists(bucketName)
      bucket = @s3Resource.bucket(bucketName)
      bucket.objects.limit(100).each do |item|
        ec2List.push(item.key) #item.public_url
      end
    end
    return ec2List
  end

#getObjectsInAFolder retuns a list of Items in a folder within a bucket
  #retuns an array of latest 50 objects
  #Example: s3Obj.common_prefixes("test-gateway-services-nonprod-elb-logs", "reverse_proxy_elb")
  #reverse_proxy_elb/AWSLogs/AWSLogs/1212/elasticloadbalancing/us-west-1/2017/09/04/1212_elasticloadbaluqwi.log
  #reverse_proxy_elb/AWSLogs/AWSLogs/1212/elasticloadbalancing/u-west-1/2017/09/06/1212_elasticloadbalan2_3228v1mv.log
  def getObjectsInAFolder(bucketName, folderName)
    bucket = @s3Resource.bucket(bucketName)
    objArray =  bucket.objects(prefix: folderName).collect(&:key)
    return objArray.reverse[0..50]
  end

  #readS3Object reads an S3 object into a file my-object-content.txt
  #s3Obj.readS3Object('test-gateway-services-nonprod-elb-logs', "reverse_proxy_elb/AWSLogs/AWSLogs/761099892790/elasticloadbalancing/eu-west-1/2017/09/06/761099892790_elasticloadbalancing_eu-west-1_ukdcouk-sys-external_20170906T0900Z_10.63.37.82_3228v1mv.log")
  def readS3Object(accountVPC, bucketName, objectName)
    obj = @s3Resource.bucket(bucketName).object(objectName)
    obj.get(response_target: 'my-object-content.txt')
  end

  #retrieveAndReadS3Object reads an S3 object into a file, and returns contents
  #In order to identify the object; accountVPC, bucketName, folderName, fileNameSubstring are required
  #Sub routine tries to find a file with object name equals filename-sub-string inside given bucket and folder
  #Contents of latest object with fileNameSubstring in filename is returned
  #s3Obj.retrieveAndReadS3Object('test-gateway-services-nonprod-elb-logs', "reverse_proxy_elb", "ukdcouk-sys-external")
  def retrieveAndReadS3Object(bucketName, folderName, fileNameSubstring)
    bucket = @s3Resource.bucket(bucketName)
    objArray =  bucket.objects(prefix: folderName).collect(&:key)
    objArray = objArray.reverse[0..500]
    objArray.each do |i|
      if i.include?fileNameSubstring
        fileContent =  @s3Client.get_object(bucket: bucketName, key: i) #Read into a file - obj = s3Resource.bucket(bucketName).object(i).get(response_target: 'my-item.txt')
        return fileContent.body.read
      end
    end
    raise Exception.new("Object with FileNameSubstring #{fileNameSubstring} not found in Folder #{folderName} on bucket #{bucketName} on accountVPC #{accountVPC}")
  end
end
