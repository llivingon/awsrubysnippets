class Route53Helper
  def initialize()
    @route53Client = Aws::Route53::Client.new()
  end

  def getZoneIdForGatewayServices(zoneIdComment="Zone for gateway services CNAMES")
    hostedZoneId = ""
    @route53Client.list_hosted_zones.to_h[:hosted_zones].each do |i|
      if i[:config][:comment] == zoneIdComment
        hostedZoneId = i[:id]
      end
    end
    return hostedZoneId
  end


  def getRecordSetsForZone(zoneId)
    return @route53Client.list_resource_record_sets({hosted_zone_id: zoneId, }).to_h[:resource_record_sets]
  end

  #getRecordSetNamesMappedToIps returns an array of record list mapped to Ips
  def getRecordSetNamesMappedToIps(ipList)
    recordsList = []
    recordSets = getRecordSetsForZone(getZoneIdForGatewayServices())
    ipList.each do |i|
      recordSets.each do |j|
        if j[:resource_records].to_s.include?i
          recordsList.push(j[:name])
        end
      end
    end
    return recordsList
  end

end
