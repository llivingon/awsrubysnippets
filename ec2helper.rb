class EC2Helper
  def initialize()
    @ec2Resource = Aws::EC2::Resource.new()
    @ec2Client = Aws::EC2::Client.new()
  end

  #getAllEc2InstancesOnAVPC returns a hash of all EC2 Instances and it's respective state in a VPC
  #Example: ec2Obj.getAllEc2InstancesOnAVPC()
  #Returns=> {"i-0651bea8845c8ace6"=>"running", "i-0457d489cf08e5ac2"=>"running", "i-061817974a864695e"=>"running"}
  def getAllEc2InstancesOnAVPC()
    ec2Hash = Hash.new(0)
    @ec2Resource.instances.each do |i|
      ec2Hash[i.id.to_s] = i.state.to_h[:name]
    end
    return ec2Hash
  end

  #getAllSecurityGroupsOnAVPC returns an array EC2 security group in a VPC
  #Example: ec2Obj.getAllEc2SecurityGroupsOnAVPC()
  #Returns => {:description=>"Security group for a specific fuse cluster", :group_name=>"fuseesb-cluster3-staging", :ip_permissions=>[], :owner_id=>"300820918606", :group_id=>"sg-f33b8b8b", :ip_permissions_egress=>[{:from_port=>47550, :ip_protocol=>"tcp", :ip_ranges=>[{:cidr_ip=>"52.208.85.231/32"}], :ipv_6_ranges=>[], :prefix_list_ids=>[], :to_port=>47550, :user_id_group_pairs=>[]}], :tags=>[{:key=>"Name", :value=>"fuseesb-cluster3-staging"}, {:key=>"team", :value=>"digital-integration"}], :vpc_id=>"vpc-0e03936a"}
  #          {:description=>"Direct access to Jenkins Master", :group_name=>"jenkins-master-direct-dig_web_mob", :ip_permissions=>[{:from_port=>22, :ip_protocol=>"tcp", :ip_ranges=>[{:cidr_ip=>"10.0.0.0/8"}], :ipv_6_ranges=>[], :prefix_list_ids=>[], :to_port=>22, :user_id_group_pairs=>[]}, {:from_port=>-1, :ip_protocol=>"icmp", :ip_ranges=>[{:cidr_ip=>"10.0.0.0/8"}], :ipv_6_ranges=>[], :prefix_list_ids=>[], :to_port=>-1, :user_id_group_pairs=>[]}], :owner_id=>"300820918606", :group_id=>"sg-f390328a",
  #          :ip_permissions_egress=>[], :tags=>[{:key=>"Costcentre_Projectcode", :value=>"9SD14"}, {:key=>"team", :value=>"digital-web-mobile"}, {:key=>"Name", :value=>"jenkins-master-direct-dig_web_mob"}, {:key=>"Owner", :value=>"fraser.goffin@test.com"}], :vpc_id=>"vpc-0e03936a"}
  def getAllEc2SecurityGroupsOnAVPC()
    return @ec2Client.describe_security_groups.to_h[:security_groups]
  end

  #getAllEc2NaclsOnAVPC returns an array of NACLS on a VPC
  #Example: ec2Obj.getAllEc2NaclsOnAVPC()
  #Returns=> ['acl-2844114c', 'acl-36ahe625', 'acl-4942126a', 'acl-56343515']
  def getAllEc2NaclsOnAVPC()
    naclIds = []
    nacls =  @ec2Client.describe_network_acls.to_h[:network_acls]
    nacls.each do |i|
      naclIds.push(i[:network_acl_id])  
    end
    return naclIds
  end
  
  #getVpcId returns VPCId for an EC2 Instance. Throws Exception if Instance not found
  #Example: ec2Obj.getVpcId("i-0651bea8845c8ace6")
  #Returns=> vpc-0e03936a
  def getVpcId(ec2Instance)
    @ec2Resource.instances.each do |i|
      if ec2Instance.include?i.id.to_s
         return i.vpc_id
      end
    end
    raise Exception.new("EC2 Instance- #{ec2Instance} is not available on VPC")
  end

  #getSubnetId returns SubnetId for an EC2 Instance. Throws Exception if Instance not found
  #Example: ec2Obj.getSubnetId("i-0651bea8845c8ace6")
  #Returns=> subnet-8ed094ea
  def getSubnetId(ec2Instance)
    @ec2Resource.instances.each do |i|
      if ec2Instance.include?i.id.to_s
         return i.subnet_id
      end
    end
    raise Exception.new("EC2 Instance- #{ec2Instance} is not available on VPC")
  end

  #getPrivateDnsName returns DnsName for an EC2 Instance. Throws Exception if Instance not found
  #Example: ec2Obj.getPrivateDnsName("i-0651bea8845c8ace6")
  #Returns=> ip-10-63-28-83.eu-west-1.compute.internal
  def getPrivateDnsName(ec2Instance)
    @ec2Resource.instances.each do |i|
      if ec2Instance.include?i.id.to_s
         return i.private_dns_name
      end
    end
    raise Exception.new("EC2 Instance- #{ec2Instance} is not available on VPC")
  end

  #getSecurityGroups returns a hash of EC2 Instance and Security group Ids to which ec2 instance is tagged to.
  #This method Throws Exception if Instance not found
  #Example: ec2Obj.getSecurityGroups("i-0651bea8845c8ace6")
  #Returns=> {"i-0651bea8845c8ace6"=>["sg-1e247267", "sg-b12472c8", "sg-3eb24358"]}
  def getSecurityGroups(ec2Instance)
    ec2Hash = Hash.new(0)
    @ec2Resource.instances.each do |i|
      if ec2Instance.include?i.id.to_s
        securityGroups = []
        i.security_groups.each do |i|
          securityGroups.push(i.to_h[:group_id])
        end
        ec2Hash[i.id.to_s] = securityGroups
        return ec2Hash
      end
    end
    raise Exception.new("EC2 Instance- #{ec2Instance} is not available on VPC")
  end
  
  #getEC2IDsFilterByTagName returns an array of all EC2 instances which has testString as it's tagName
  #Example: ec2Obj.getEC2IDsFilterByTagName('jboss')
  #Returns=> ["i-0651bea8845c8ace6","i-0651bea8845c8ace6"]
  def getEC2IDsFilterByTagName(testString)
    ec2List = []
    @ec2Resource.instances({filters: [{name: 'tag:Name', values: [testString]}]}).each do |i|
      ec2List.push(i.id)
    end
    return ec2List
  end

  #getEC2IDsFilterByTagNameSubstring returns an array of all EC2 instances which has testSubstring in it's tagName
  #Example: ec2Obj.getEC2IDsFilterByTagNameSubstring('jboss')
  #Returns=> ["i-0651bea8845c8ace6","i-0651bea8845c8ace6"]
  def getEC2IDsFilterByTagNameSubstring(testSubstring)
    ec2List = []
    @ec2Resource.instances.each do |i|
      i.tags.each do |tag|
        if  tag.key == 'Name'
          if tag.value.include? testSubstring
            ec2List.push(i.id)
          end  
        end
      end
    end  
    return ec2List
  end
  
  #getRouteTableId returns RouteTable to which ec2 instance's subnet belongs to.
  #This method Throws Exception if Instance not found
  #Example: ec2Obj.getRouteTableInfo("i-0651bea8845c8ace6")
  #Returns=> rtb-22abc21d
  def getRouteTableId(ec2Instance)
    rt = @ec2Client.describe_route_tables()
    @ec2Resource.instances.each do |i|
      if i.state.to_h[:name] == "running"
        if ec2Instance.include?i.id.to_s
          rt.to_h[:route_tables].each do |j|
            j[:associations].each do |k|
              if k[:subnet_id] == i.subnet_id
                return k[:route_table_id]
              end
            end
          end
        end
      end
    end
    raise Exception.new("EC2 Instance- #{ec2Instance} is not available on VPC-")
  end

  #getSubnetsAssociationsForNacl returns an array of SubnetID Assosciations for a NACL on a VPC
  #Example: ec2Obj.getSubnetsAssociationsForNacl('acl-1212112')
  #Returns=> ['subnet-312b980b','subnet-123122b980b','subnet-123b980b']
  def getSubnetsAssociationsForNacl(naclId)
    subnetIds = []
    @ec2Client.describe_network_acls(network_acl_ids: [naclId]).to_h[:network_acls][0][:associations].each do |i|
      subnetIds.push(i[:subnet_id])
    end
    return subnetIds
  end
  
  #getSubnetsAssociationsForAllNaclsOnVPC returns a hash of all NACLs and theirs respective subnet associations on a VPC
  #Example: ec2Obj.getSubnetsAssociationsForAllNaclsOnVPC()
  #Returns: {"acl-2844114c"=>["subnet-1212b980b", "subnet-01dbaa12"], "acl-22abe625"=>["subnet-22ccae15"]}
  def getSubnetsAssociationsForAllNaclsOnVPC()
    naclHash = Hash.new()
    @ec2Client.describe_network_acls().to_h[:network_acls].each do |i|
      i[:associations].each do |j|
        if (naclHash.key?(j[:network_acl_id]))
          naclHash[j[:network_acl_id]] = naclHash[j[:network_acl_id]].push(j[:subnet_id])
        else    
          naclHash[j[:network_acl_id]] = [j[:subnet_id]]
        end    
      end
    end
    return naclHash 
  end
  
  #getSubnetDetails returns a hash of Subnet details for given subnet-id. 
  #This method Throws Exception if Subnet not found
  #Example: ec2Obj.getSubnetDetails('subnet-1212')
  #Returns=> {:availability_zone=>"us-west-1b", :available_ip_address_count=>12, :cidr_block=>"192.63.26.0/22", :default_for_az=>false, :map_public_ip_on_launch=>false, :state=>"available", 
  #           :subnet_id=>"subnet-1212", :vpc_id=>"vpc-1212", :assign_ipv_6_address_on_creation=>false, :ipv_6_cidr_block_association_set=>[], :tags=>[{:key=>"Name", :value=>"orange"}]}
  def getSubnetDetails(subnetId)
    subnetDetails = @ec2Client.describe_subnets({filters: [{name: 'subnet-id', values: [subnetId]}]}).to_h[:subnets][0]
    unless subnetDetails.empty?
      return subnetDetails 
    end
    raise Exception.new("Subnet- #{subnetId} is not available on VPC")
  end
end

